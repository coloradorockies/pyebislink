**pyEbisLink**

A Python package to interact with the MLB eBisLink API.

https://ebislink.mlb.com/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

---

## Requirements

1. Install [Python 3](https://www.python.org/)
2. Install [pip](https://pip.pypa.io/en/stable/installing/)
3. Gain access to the Rockies pyEbisLink repository on bitbucket

---

## Installation

The package can be installed via 'pip' using [either git+https, or git+ssh](https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support)

1. Install via https and account credentials (requires user to input credentials to install)
	```
	pip install git+https://bitbucket.org/coloradorockies/pyebislink.git
	```
2. Install via ssh if ssh keys are configured for your bitbucket account and computer
	```
	pip install git+ssh://git@bitbucket.org/coloradorockies/pyebislink.git
	```

To upgrade, run either of the previous commands with the ```--upgrade``` option

---

## Usage / Getting Started

Here is a super simple example
```python
# Import the package
import pyEbisLink

# Create a link
link= pyEbisLink.EbisLink('first.last@rockies.com','mySecretPassword')

# The the status of the API
print(link.Status())

# Close the Link (REQUIRED - Otherwise the script with not exit and keep the connection open)
link.close()
```

---

## API

[Official eBisLink Swagger Documentation](https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html)


##### Status Endpoint
```python

# /status
link.Status(count=False)

```


##### Lookup Endpoint
```python

# /lkup
link.LookupTypes()

# /lkup/{lkup_type}
link.LookupsByType(lkup_type, offset=0, limit=1000, effectiveDate=None)

# /lkup/{lkup_type}/updated
link.UpdatedLookupsByType(lkup_type, fromDate=None, toDate=None, offset=0, limit=1000)

# /lkup/{lkup_type}/{lkup_id}
link.Lookup(lkup_type, lkup_id, effectiveDate=None)
```


##### Person Endpoints
```python

# /person/{person_id}
link.Person(person_id) 

# /person/updated
link.PersonUpdated( 
	fromDate=None, toDate=None, orgId=0, offset=0, limit=500,
	excludeService=False, currentType=None,
	onRoster=None, on40ManRoster=None, onMnLeagueRoster=None)

# /person/all
link.PersonAll(
	offset=0, limit=500, orgId=0,
	currentType=None, onRoster=None,
	on40ManRoster=None, onMnLeagueRoster=None)

```


##### Transaction Endpoints
```python

# /tx/{txId}
link.Transaction(txId) 

# /tx/player/{playerId}
link.TransactionForPlayer(playerId)

# /tx/all
link.TransactionAll(offset=0, limit=1000)

# /tx/updated
link.TransactionUpdated(romDate=None, toDate=None, offset=0, limit=1000)
```


##### Major League Contract Endpoints
```python
# /contract/major/{mjcId}
link.MajorContract(mjcId)

# /contract/major/{playerId}
link.MajorContractForPlayer(playerId)

# /contract/major/all
link.MajorContractAll(offset=0, limit=1000, currentFlag=True)

# /contract/major/updated
link.MajorContractUpdated(fromDate=None, toDate=None, offset=0, limit=1000, currentFlag=True)
```


##### Minor League Contract Endpoints
```python
# /contract/minor/player/{playerId}
link.MinorContractForPlayer(playerId)

# /contract/minor/all
link.MinorContractAll(offset=0, limit=1000, currentFlag=True)

# /contract/minor/{mncId}
link.MinorContract(mncId)

# /contract/minor/updated
link.MinorContractUpdated(fromDate=None, toDate=None, offset=0, limit=1000, currentFlag=True)
```


##### Addendum C Endpoints
```python
# /addc/{addCId}
link.AddC(addCId)

# /addc/player/{playerId}
link.AddCForPlayer(playerId)

# /addc/all
link.AddCAll(offset=0, limit=1000)

# /addc/contract/{mncId}
link.AddCByMnContract(mncId)

# /addc/all/deleted
link.AddCDeleted(offset=0, limit=1000)

# /addc/updated
link.AddCUpdated(fromDate=None, toDate=None, offset=0, limit=1000, currentFlag=True)
```


##### Addendum D Endpoints
```python
# /addd/{addDId}
link.AddD(addDId)

# /addd/player/{playerId}
link.AddDForPlayer(playerId)

# /addd/all
link.AddDAll(offset=0, limit=1000, currentFlag=True)

# /addd/contract/{mncId}
link.AddDByMnContract(mncId)

# /addd/updated
link.AddDUpdated(fromDate=None, toDate=None, offset=0, limit=1000, currentFlag=True)
```


##### Arbitration Endpoints
```python
# /arb/{year}
link.ArbitrationByYear(year, offset=0, limit=1000)

# /arb/player/{playerId}
link.ArbitrationForPlayer(playerId)

# /arb/all
link.ArbitrationAll(offset=0, limit=1000)

# /arb/updated
link.ArbitrationUpdated(fromDate=None, toDate=None, offset=0, limit=1000)
```


##### Award Endpoints
```python
# /awards/{awardCode}/year/{year}
link.AwardByYearAndCode(awardCode, year)

# /awards/player/{playerId}
link.AwardForPlayer(playerId)

# /awards/all
link.AwardAll(offset=0, limit=500)

# /awards/updated
link.AwardUpdated(fromDate=None, toDate=None, offset=0, limit=1000)
```


##### Transaction Bulletin Endpoints
```python
# /bulletin/{bulletinId}
link.TransactionBulletin(bulletinId)

# /bulletin/updated
link.TransactionBulletinUpdated(fromDate=None, toDate=None, offset=0, limit=1000)
```


##### Rule5 Endpoint
```python
# /rule5/players
link.RuleFive()
```

##### Daily Roster Endpoints
```python
# /rostersnapshot/person/{personId}/fromDate/{fromDate}
link.DailyRosterForPerson(personId, fromDate)

# /rostersnapshot/{orgId}/date/{rosterDate}
link.DailyRosterForOrg(orgId, rosterDate)
```


##### Special Roster Endpoint
```python
# /specialRoster/{specialRosterTypeId}/year/{year}/org/{orgId}
link.SpecialRosterByYearAndOrg(specialRosterTypeId, year, orgId, fromDate=None, toDate=None, fromDateMs=None, toDateMs=None)
```