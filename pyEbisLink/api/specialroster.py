# Special Roster
# eBis Link Special Roster API
# https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

import datetime

# /specialRoster/{specialRosterTypeId}/year/{year}/org/{orgId}
# Retrieve a List of Special Rosters by year and org
def SpecialRosterByYearAndOrg(self, specialRosterTypeId, year, orgId, fromDate=None, toDate=None, fromDateMs=None, toDateMs=None):
	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {
		'fromDate': fromDate,
		'fromDateMs': fromDateMs,
		'toDate': toDate,
		'toDateMs': toDateMs
	}

	if toDate is not None:
		params['toDate']= toDate
	if toDateMs is not None:
		params['toDateMs']= toDateMs
	if fromDate is not None:
		params['fromDate']= fromDate
	if fromDateMs is not None:
		params['fromDateMs']= fromDateMs

	path = "/v1/specialRoster/%s/year/%s/org/%s" % (specialRosterTypeId,year,orgId)
	return self._apiRequest(path, params)