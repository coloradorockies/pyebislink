# AddendumD
# eBis Link AddendumD API
# https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

import datetime

# /addd/{addDId}
# Retrieve the Addendum D API model by addDId
def AddD(self, addDId):
	path = "/v1/addd/%s" % addDId
	return self._apiRequest(path)

# /addd/player/{playerId}
# Retrieve all the Addendum D API models by playerId
def AddDForPlayer(self, playerId):
	path = "/v1/addd/player/%s" % playerId
	return self._apiRequest(path)

# /addd/all
# Retrieve all the Addendum D API models
def AddDAll(self, offset=0, limit=1000):
	# Set up request params
	params = {'offset': offset, 'limit': limit}

	# Send the request
	path = "/v1/addd/all"
	return self._apiRequest(path, params)

# /addd/contract/{mncId}
# Retrieve all the Addendum D API models by mncId
def AddDByMnContract(self, mncId):
	path = "/v1/addd/contract/%s" % mncId
	return self._apiRequest(path)

# /addd/updated
# Retrieve a List of Addendum D models that have been updated within the specified time period.
def AddDUpdated(self, fromDate=None, toDate=None, offset=0, limit=1000,  cacheUpdatedTimestamp=True):
	# Set up request params
	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {'fromDate': fromDate, 'offset': offset, 'limit': limit,  'cacheUpdatedTimestamp': cacheUpdatedTimestamp}

	if toDate is not None:
		params['toDate']= toDate

	# Send the request
	path = "/v1/addd/updated"
	return self._apiRequest(path, params)