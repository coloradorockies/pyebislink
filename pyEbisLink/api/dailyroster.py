# Daily Roster
# eBis Link Daily Roster API
# https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

import datetime

# /rostersnapshot/person/{personId}/fromDate/{fromDate}
# Retrieve roster for a person and roster date
def DailyRosterForPerson(self, personId, fromDate):
	#.     https://ebislink.mlb.com/clubs/4hSWpbwprbmS5AQ6/v1/rostersnapshot/person/
	#    URL = f"{baseURL}{str(player_personid)}?fromDate={fromDate}"
	#addvv1 before roster
	path = "/v1/rostersnapshot/person/%s?fromDate=%s" % (personId,fromDate)
	return self._apiRequest(path)

# /rostersnapshot/{orgId}/date/{rosterDate}
# Retrieve roster for supplied org id and roster date
def DailyRosterForOrg(self, orgId, rosterDate):
	path = "/v1/rostersnapshot/org/%s?rosterDate=%s" % (orgId,rosterDate)
	return self._apiRequest(path)