# Arbitration
# eBis Link Arbitration API
# https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

import datetime

# /arb/{year}
# Retrieve the Arbitration Api model by Year
def ArbitrationByYear(self, year, offset=0, limit=1000):
	#Setup request params
	params = {'offset': offset, 'limit': limit}
	path = "/v1/arb/%s" % year
	return self._apiRequest(path, params)

# /arb/all
# Retrieve a List of all Arbitration Api models
def ArbitrationAll(self, offset=0, limit=1000):
	#Setup request params
	params = {'offset': offset, 'limit': limit}

	#Send the request
	path = "/v1/arb/all"
	return self._apiRequest(path, params)

# /arb/player/{playerId}
# Retrieve the Arbitration Api model by Player ID
def ArbitrationForPlayer(self, playerId):
	path = "/v1/arb/player/%s" % playerId
	return self._apiRequest(path)

# /arb/updated
# Retrieve a List of Arbitrations that have been updated within a specific time period
def ArbitrationUpdated(self, fromDate=None, toDate=None, offset=0, limit=1000, cacheUpdatedTimestamp=True):
	# Set up request params
	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {'fromDate': fromDate, 'offset': offset, 'limit': limit, 'cacheUpdatedTimestamp': cacheUpdatedTimestamp}

	if toDate is not None:
		params['toDate']= toDate

	# Send the request
	path = "/v1/arb/updated"
	return self._apiRequest(path, params)