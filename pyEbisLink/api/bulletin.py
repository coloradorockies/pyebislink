# Transaction Bulletin
# eBis Link Transaction Bulletin API
# https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

import datetime

# /bulletin/{bulletinId}
# Retrieve the Bulletin Api model by Bulletin Id
def TransactionBulletin(self, bulletinId):
	path= "/v1/bulletin/%s" % bulletinId
	return self._apiRequest(path)

# /bulletin/updated
# Retrieve a List of Bulletins that have been updated within the specified time period.
def TransactionBulletinUpdated(self, fromDate=None, toDate=None, offset=0, limit=1000, cacheUpdatedTimestamp=True):
	# Set up request params
	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {
		'fromDate': fromDate,
		'offset': offset,
		'limit': limit,
		'cacheUpdatedTimestamp': cacheUpdatedTimestamp
	}

	if toDate is not None:
		params['toDate']= toDate

	# Send the request
	path= "/v1/bulletin/updated"
	return self._apiRequest(path, params)
