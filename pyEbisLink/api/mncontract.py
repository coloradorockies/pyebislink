# Minor League Contracts
# eBis Link Minor League Contract API
# https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

import datetime

# /contract/minor/{mncId}
# Get a specific contract by id.
def MinorContract(self, mncId):
	path= "/v1/contract/minor/%s" % mncId
	return self._apiRequest(path)

# /contract/minor/{playerId}
# Get all transactions for a specific player.
def MinorContractForPlayer(self, playerId):
	path= "/v1/contract/minor/player/%s" % playerId
	return self._apiRequest(path)

# /contract/minor/all
# Get all transactions; Limited to 1000 records at a time
def MinorContractAll(self, offset=0, limit=1000):
	# Set up request params
	params = {'offset': offset, 'limit': limit}

	# Send the request
	path= "/v1/contract/minor/all"
	return self._apiRequest(path, params)

# /contract/major/updated
# Get all contracts updated within a date range
def MinorContractUpdated(self, fromDate=None, toDate=None, offset=0, limit=1000, cacheUpdatedTimestamp=True):
	# Set up request params
	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {
		'fromDate': fromDate,
		'offset': offset,
		'limit': limit,
		'cacheUpdatedTimestamp': cacheUpdatedTimestamp
	}

	if toDate is not None:
		params['toDate']= toDate

	# Send the request
	path= "/v1/contract/minor/updated"
	return self._apiRequest(path, params)
