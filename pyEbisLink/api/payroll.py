# Payroll
# eBis Link Payroll API
# https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

import datetime

# /payroll/{asOf}/{year}
# Retrieve the Payroll Api model
def Payroll(self, asOf, year, team=None):
	params= {}
	if team is not None:
		params['team']= team

	path = "/v1/payroll/%s/%s" % (asOf, year)
	return self._apiRequest(path,params)
