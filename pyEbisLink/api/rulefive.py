# Rule 5
# eBis Link Rule5 API
# https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

# /rule5/players
# Retrieve the list of R5 Selectable Player Api models
def RuleFive(self):
	path= "/v1/rule5/players"
	return self._apiRequest(path)
