"""
eBis Link Transaction API
https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html
"""

import datetime


def Transaction(self, txId):
	"""
	/tx/{txId}

	Get a specific transaction
	"""
	path= "/v1/tx/%s" % txId
	return self._apiRequest(path)


def TransactionForPlayer(self, playerId):
	"""
	/tx/player/{playerId}
	Get all transactions for a specific player
	"""
	path= "/v1/tx/player/%s" % playerId
	return self._apiRequest(path)


def TransactionAll(self, offset=0, limit=1000):
	"""
	/tx/all
	Get all transactions; Limited to 1000 records at a time
	"""

	# Set up request params
	params= {'offset': offset, 'limit': limit}

	# Send the request
	path= "/v1/tx/all"
	return self._apiRequest(path, params)


def TransactionUpdated(self, fromDate=None, toDate=None, offset=0, limit=1000, cacheUpdatedTimestamp=True):
	"""
	/tx/updated
	Get all transactions updated within a date range
	"""

	# Set up request params
	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {
		'fromDate': fromDate,
		'offset': offset,
		'limit': limit,
		'cacheUpdatedTimestamp': cacheUpdatedTimestamp
	}

	if toDate is not None:
		params['toDate']= toDate

	# Send the request
	path= "/v1/tx/updated"
	return self._apiRequest(path, params)
