"""
eBis Link Major League Contract API
https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html
"""

import datetime


def MajorContract(self, mjcId):
	"""
	/contract/major/{mjcId}

	Get a specific contract by id.
	"""

	path= "/v1/contract/major/%s" % mjcId
	return self._apiRequest(path)


def MajorContractForPlayer(self, playerId):
	"""
	/contract/major/{playerId}

	Get all transactions for a specific player.
	"""

	path= "/v1/contract/major/player/%s" % playerId
	return self._apiRequest(path)


def MajorContractAll(self, offset=0, limit=1000):
	"""
	/contract/major/all

	Get all transactions; Limited to 1000 records at a time
	"""

	# Set up request params
	params = {'offset': offset, 'limit': limit}

	# Send the request
	path= "/v1/contract/major/all"
	return self._apiRequest(path, params)


def MajorContractUpdated(self, fromDate=None, toDate=None, offset=0, limit=1000,  cacheUpdatedTimestamp=True):
	"""
	/contract/major/updated
	Get all contracts updated within a date range
	"""

	# Set up request params
	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {
		'fromDate': fromDate,
		'offset': offset,
		'limit': limit,
		'cacheUpdatedTimestamp': cacheUpdatedTimestamp
	}

	if toDate is not None:
		params['toDate']= toDate

	# Send the request
	path= "/v1/contract/major/updated"
	return self._apiRequest(path, params)
