"""
eBis Link Lookup API
https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html
"""
import datetime


def LookupTypes(self):
	"""
	/lkup

	Returns the available lookups
	"""
	path= "/v1/lkup"
	return self._apiRequest(path)


def LookupsByType(self, lkup_type, offset=0, limit=1000, page=0, effectiveDate=None):
	"""
	/lkup/{lkup_type}

	Return all lookups by type (subject to limit and offset)
	"""

	# Set up request params
	params= {
		'offset': offset,
		'limit': limit,
		'page':page
	}

	if effectiveDate is not None:
		params['effectiveDate']= effectiveDate

	# Send the request
	path= "/v1/lkup/%s" % lkup_type
	return self._apiRequest(path, params)


def UpdatedLookupsByType(self, lkup_type, fromDate=None, toDate=None, offset=0, limit=1000, page=0):
	"""
	/lkup/{lkup_type}/updated

	Return all updated lookups between two dates
	"""

	# Set up request params
	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {
		'fromDate': fromDate,
		'offset': offset,
		'limit': limit,
		'page':page
	}

	if toDate is not None:
		params['toDate']= toDate

	# Send the request
	path= "/v1/lkup/%s/updated" % lkup_type
	return self._apiRequest(path, params)


def Lookup(self, lkup_type, lkup_id, effectiveDate=None):
	"""

	"""

	# Set up request params
	params= None
	if effectiveDate is not None:
		params= {'effectiveDate': effectiveDate}

	# Send the request
	path= "/v1/lkup/%s/%s" % (lkup_type, lkup_id)
	return self._apiRequest(path, params)
