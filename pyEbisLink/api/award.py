# Award
# eBis Link Awards API
# https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

import datetime

# /awards/{awardCode}/year/{year}
# Retrieve the Award Api model by Award Code and Year
def AwardByYearAndCode(self, awardCode, year):
	path = "/awards/%s/year/%s" % (awardCode, year)
	return self._apiRequest(path)

# /awards/player/{playerId}
# Retrieve the Award Api model by Player ID
def AwardForPlayer(self, playerId):
	path = "/v1/awards/player/%s" % playerId
	return self._apiRequest(path)

# /awards/all
# Retrieve a List of all Award Api models
def AwardAll(self, offset=0, limit=500):
	# Set up request params
	params = {'offset': offset, 'limit': limit}

	# Send the request
	path = "/v1/awards/all"
	return self._apiRequest(path, params)

# /awards/updated
# Retrieve a List of Awards that have been updated within the specified time period.
def AwardUpdated(self, fromDate=None, toDate=None, offset=0, limit=1000, cacheUpdatedTimestamp=True):
	# Set up request params
	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {'fromDate': fromDate, 'offset': offset, 'limit': limit, 'cacheUpdatedTimestamp': cacheUpdatedTimestamp}

	if toDate is not None:
		params['toDate']= toDate

	# Send the request
	path = "/v1/awards/updated"
	return self._apiRequest(path, params)