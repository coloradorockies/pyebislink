"""
eBis Link Person API
https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html
"""

import datetime


def Person(self, person_id):
	"""
	/person/ebisId

	Returns a specific Person record for the given ebisId

	TODO:
		- Test orgId param. Seems to not be actually filtering records
	"""

	path= "/v1/person/%s" % person_id
	return self._apiRequest(path)

#took out limit=500,
def PersonUpdated(
	self, fromDate=None,limit=500, toDate=None, orgId=0, offset=0,page=0,
	excludeService=False, currentType=None,
	onRoster=None, on40ManRoster=None, onMnLeagueRoster=None, cacheUpdatedTimestamp=True):
	"""
	/person/updated

	Returns Person records that were updated between fromDate and toDate
	"""

	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {
		'fromDate': fromDate,
		'excludeService': excludeService,
		'offset': offset,
		'limit': limit,
		'orgId': orgId,
		'cacheUpdatedTimestamp': cacheUpdatedTimestamp,
		'page':page
	}

	if toDate is not None:
		params['toDate']= toDate
	if currentType is not None:
		params['currentType']= currentType
	if onRoster is not None:
		params['onRoster']= onRoster
	if on40ManRoster is not None:
		params['on40ManRoster']= on40ManRoster
	if onMnLeagueRoster is not None:
		params['onMnLeagueRoster']= onMnLeagueRoster

	path= '/v1/person/updated'
	return self._apiRequest(path, params)


def PersonAll(
	self, offset=0, limit=500, orgId=0, page=0,
	currentType=None, onRoster=None,
	on40ManRoster=None, onMnLeagueRoster=None):
	"""
	/person/all

	Returns all Person records that meet the filters. Max 500 records returned
	"""

	params= {
		'offset': offset, 
		'limit': limit, 
		'orgId': orgId, 
		'page':page
		}

	if currentType is not None:
		params['currentType']= currentType
	if onRoster is not None:
		params['onRoster']= onRoster
	if on40ManRoster is not None:
		params['on40ManRoster']= on40ManRoster
	if onMnLeagueRoster is not None:
		params['onMnLeagueRoster']= onMnLeagueRoster

	path= '/v1/person/all'
	return self._apiRequest(path, params)
