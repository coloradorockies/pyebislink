# AddendumC
# eBis Link AddendumC API
# https://ebislink.mlb.com/ebis-link-services/clubs/4hSWpbwprbmS5AQ6/swagger-ui-clubs/index.html

import datetime

# /addc/{addCId}
# Retrieve the Addendum C Api model by Addendum C ID
def AddC(self, addCId):
	path = "/v1/addc/%s" % addCId
	return self._apiRequest(path)

# /addc/player/{playerId}
# Retrieve the Addendum C Api model by Player Id
def AddCForPlayer(self, playerId):
	path = "/v1/addc/player/%s" % playerId
	return self._apiRequest(path)

# /addc/all
# Retrieve a List of all AddendumC Api models
def AddCAll(self, offset=0, limit=1000):
	# Set up request params
	params = {'offset': offset, 'limit': limit}

	# Send the request
	path = "/v1/addc/all"
	return self._apiRequest(path, params)

# /addc/contract/{mncId}
# Retrieve the Addendum C Api model by Minor Contract Id
def AddCByMnContract(self, mncId):
	path = "/v1/addc/contract/%s" % mncId
	return self._apiRequest(path)

# /addc/all/deleted
# Retrieve the AddendumC Api Deleted model
def AddCDeleted(self, offset=0, limit=1000):
	# Set up request params
	params= {'offset': offset, 'limit': limit}

	# Send the request
	path = "/v1/addc/all/deleted"
	return self._apiRequest(path, params)

# /addc/updated
# Retrieve a List of AddendumC's that have been updated within the specified time period.
def AddCUpdated(self, fromDate=None, toDate=None, offset=0, limit=1000, cacheUpdatedTimestamp=True):
	# Set up request params
	if fromDate is None:
		fromDate= datetime.datetime(2000, 1, 1).strftime('%m/%d/%y %H:%M %p')

	params= {'fromDate': fromDate, 'offset': offset, 'limit': limit, 'cacheUpdatedTimestamp': cacheUpdatedTimestamp}

	if toDate is not None:
		params['toDate']= toDate

	# Send the request
	path = "/v1/addc/updated"
	return self._apiRequest(path, params)