import requests


# EBIS_AUTH= 'https://ebislink.mlb.com/token/access'
# EBIS_ROOT= 'https://ebislink.mlb.com/clubs/4hSWpbwprbmS5AQ6'
# EBIS_RFSH= 'https://ebislink.mlb.com/token/refresh'

# EBIS_AUTH_TRN= 'https://ebislinktrn.mlb.com/token/access'
# EBIS_ROOT_TRN= 'https://ebislinktrn.mlb.com/clubs/4hSWpbwprbmS5AQ6'
# EBIS_RFSH_TRN= 'https://ebislinktrn.mlb.com/token/refresh'

class EbisLink():
	def __init__(self, username, password, debug=False,trn=False):
		global EBIS_AUTH
		global EBIS_ROOT
		global EBIS_RFSH
		if trn == True:
			EBIS_AUTH= 'https://ebislinktrn.mlb.com/token/access'
			EBIS_ROOT= 'https://ebislinktrn.mlb.com/clubs/4hSWpbwprbmS5AQ6'
			EBIS_RFSH= 'https://ebislinktrn.mlb.com/token/refresh'
		else:
			EBIS_AUTH= 'https://ebislink.mlb.com/token/access'
			EBIS_ROOT= 'https://ebislink.mlb.com/clubs/4hSWpbwprbmS5AQ6'
			EBIS_RFSH= 'https://ebislink.mlb.com/token/refresh'
		self._username= username
		self._password= password
		self._debugLog= debug
		self._login()

	# Deprecated
	def close(self):
		return

	def _debug(self, text):
		if self._debugLog:
			print(text)

	def _authHeader(self):
		return "Bearer %s" % self._token

	def _refresh(self):
		self._debug('EbisLink._refresh: refreshing token')
		headers= {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Accept': 'application/json'
		}
		data= {
			'refresh_token': self._refresh
		}
		r= requests.post(EBIS_RFSH, data=data, headers=headers)
		if r.status_code == 200:
			res= r.json()
			self._debug('EbisLink._login: res: %s' % res)
			self._token= res['access_token']
			self._refresh= res['refresh_token']
		else:
			print('EbisLink: Unable to authenticate with the given credentials')
			self._token= ''
			raise AuthenticationError('%s: %s' % (r.status_code, r.reason))

	def _login(self):
		self._debug('EbisLink._login: loging in')
		headers= {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Accept': 'application/json'
		}
		data= {
			'username': self._username,
			'password': self._password
		}
		r= requests.post(EBIS_AUTH, data=data, headers=headers)
		if r.status_code == 200:
			res= r.json()
			self._debug('EbisLink._login: res: %s' % res)
			self._token= res['access_token']
			self._refresh= res['refresh_token']
		else:
			print('EbisLink: Unable to authenticate with the given credentials')
			self._token= ''
			raise AuthenticationError('%s: %s' % (r.status_code, r.reason))

	def _apiRequest(self, path, params=None):
		url= '%s%s' % (EBIS_ROOT, path)
		headers= {
			'Accept': 'application/json',
			'Authorization': self._authHeader()
		}
		r= requests.get(url, params=params, headers=headers)
		if r.status_code == 200:
			return r.json()
		elif r.status_code == 401:
			self._debug('Got Expired Token. Refreshing Token and trying again')
			self._refresh()
			self._apiRequest(path, params)
		else:
			raise ApiRequestError('%s: %s:' % (r.status_code, r.text))

	### API Endpoints ###

	# Status
	from .api.status import Status

	# Person
	from .api.person import Person
	from .api.person import PersonAll
	from .api.person import PersonUpdated

	# Lookup
	from .api.lookup import Lookup
	from .api.lookup import LookupTypes
	from .api.lookup import LookupsByType
	from .api.lookup import UpdatedLookupsByType

	# Transaction
	from .api.transaction import Transaction
	from .api.transaction import TransactionAll
	from .api.transaction import TransactionUpdated
	from .api.transaction import TransactionForPlayer

	# Major League Contracts
	from .api.mjcontract import MajorContract
	from .api.mjcontract import MajorContractAll
	from .api.mjcontract import MajorContractUpdated
	from .api.mjcontract import MajorContractForPlayer

	# Minor League Contractseeeee
	from .api.mncontract import MinorContract
	from .api.mncontract import MinorContractAll
	from .api.mncontract import MinorContractUpdated
	from .api.mncontract import MinorContractForPlayer

	# Rule5
	from .api.rulefive import RuleFive

	# Transaction Bulletin
	from .api.bulletin import TransactionBulletin
	from .api.bulletin import TransactionBulletinUpdated

	# Arbitration
	from .api.arb import ArbitrationByYear
	from .api.arb import ArbitrationAll
	from .api.arb import ArbitrationForPlayer
	from .api.arb import ArbitrationUpdated

	# AddendumC
	from .api.addc import AddC
	from .api.addc import AddCAll
	from .api.addc import AddCDeleted
	from .api.addc import AddCForPlayer
	from .api.addc import AddCByMnContract
	from .api.addc import AddCUpdated

	# Award
	from .api.award import AwardAll
	from .api.award import AwardForPlayer
	from .api.award import AwardUpdated
	from .api.award import AwardByYearAndCode

	# AddendumD
	from .api.addd import AddD
	from .api.addd import AddDAll
	from .api.addd import AddDByMnContract
	from .api.addd import AddDForPlayer
	from .api.addd import AddDUpdated

	# Payroll
	from .api.payroll import Payroll

	# Daily Roster
	from .api.dailyroster import DailyRosterForPerson
	from .api.dailyroster import DailyRosterForOrg

	# Special Roster
	from .api.specialroster import SpecialRosterByYearAndOrg

class AuthenticationError(Exception):
	pass


class ApiRequestError(Exception):
	pass