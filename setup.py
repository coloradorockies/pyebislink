
from setuptools import setup, find_packages

setup(
	name='pyEbisLink',
	version='0.6.4.1',
	packages=find_packages(),
	description='Python Library for connection to the MLB eBis Link API v1',
	author='Jamie Hollowell, Declan Costello',
	author_email='Declan.Costello@rockies.com',
	install_requires=['requests>=2.19']
)
